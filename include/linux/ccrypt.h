/* 
 * Cheap crypt (ccrypt).
 * (C) 2006 Dawid Ciezarkiewicz <dpc@asn.pl>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifdef __KERNEL__
#ifndef __CCRYPT_H__
#define __CCRYPT_H__
#ifdef CONFIG_NETDEV_CCRYPT

#include <linux/crypto.h>

struct ccrypt_rx
{
	/* tfms[0] - "new" key */
	/* tfms[1] - "old" key */
	struct crypto_blkcipher* tfms[2];

	/* [key][0] => iv from last good received packet */
	/* [key][1] => iv from last received packet */
	u8* last_recv_iv[2][2];

	/* are last_recv_iv[key][0] and [key][1] equal? */
	u8 last_recv_iv_matched[2];

	/* should receiver use reversed order of keys
	 * until sender starts using new key? */
	u8 after_switch;

	/* counters used to report possible spoof attacks */
	u16 valid_counter;
	u8 invalid_counter;
};


struct ccrypt_tx
{
	struct crypto_blkcipher* tfm;
	u8* last_sent_iv;
};

struct sk_buff;
struct device;
struct net_device;
struct device_attribute;

int ccrypt_encrypt(struct sk_buff **pskb);
int ccrypt_decrypt(struct sk_buff **pskb);
ssize_t ccrypt_rx_store(struct device *dev, struct device_attribute *attr,
	const char *buf, size_t len);
ssize_t ccrypt_tx_store(struct device *dev, struct device_attribute *attr,
	const char *buf, size_t len);
ssize_t ccrypt_rx_show(struct device *dev, struct device_attribute *attr,
	char *buf);
ssize_t ccrypt_tx_show(struct device *dev, struct device_attribute *attr,
	char *buf);
void ccrypt_tx_reset(struct net_device* dev);
void ccrypt_rx_reset(struct net_device* dev);
#endif /* CONFIG_NETDEV_CCRYPT */
#endif /* __CCRYPT_H__ */
#endif /* __KERNEL__ */
